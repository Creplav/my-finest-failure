package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import sample.plants.RedAlgae;
import sample.animals.vertebrates.HammerheadShark;

import java.net.URL;
import java.util.*;

/**
 * This is the controller for the program.
 * It handles all of the FXML data and general program structure.
 * Potentially be broken up into a separate class.
 */
public class ControllerOld implements Initializable{

    /*
     * Reference all of the objects in the FXML file
     * That way we can use it later in the program
     */
    public TextArea textArea;
    public Button button1;
    public Button button2;

    //Create a list of researchables that we can use in the program
    public ArrayList<Researchable> researchableArrayList = new ArrayList<>();

    public HashMap<Button, Researchable> buttonMap = new HashMap<>();
    //public HashMap<Integer, String> choiceMap = new HashMap<>();

    //Create some organisms to put in the list
    private RedAlgae redAlgae = new RedAlgae();
    private HammerheadShark hammerheadShark = new HammerheadShark();

    //Create a boolean for checking if the game is over
    private boolean gameOver = false;

    //Init method to handle the program loading
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //Start the story with some really cheesy wording
        textArea.setText("You move to the edge of the boat. \n\"Lower the ROV\", you say. " +
                "\nThe ROV is lowered into the water and you take the controls. It is a normal exploration mission. " +
                "Just a simple data collection mission. You begin your decent.... \n");
        //Add some preliminary items to the list
        researchableArrayList.add(redAlgae);
        researchableArrayList.add(hammerheadShark);
        //Update the UI to make sure that everything looks nice.
       updateButtons();
    }

    /**
     * This method handles button clicks based on an ActionEvent assigned to the buttons inside the FXML.
     * It checks to make sure which button you are pressing in order to continue the story
     */
    @FXML
    public void handleButtonClick(ActionEvent event){
        //Create a new button we can use as our clicked button
        Button clickedButton = new Button();
        //Create a new object that we will use for casting to the clicked button
        Object source = event.getSource();
        //Make sure that the ActionEvent is type of button
        if(source instanceof Button){
            //Cast the object to a button and set clickedButton equal to that
            clickedButton = (Button) source;
        }
        //Check to see if the clicked button was 1 or 2
        if(clickedButton.getId().equals("button1")){
            progressStory(1);
        }
        else if(clickedButton.getId().equals("button2")){
            progressStory(2);
        }
        //If it wasn't, something went wrong and we need to identify the problem.
        else {
            System.out.println("This doesn't work idiot.");
            System.out.println(event.getSource().toString());
            System.out.println(event.getEventType().getName());
            System.out.println(event.toString());
            System.out.println(event.getClass());
        }
    }

    /**
     * This method checks to see if the collection is empty. Once complete it returns boolean value
     * @param collection
     * @return
     */
    public static boolean isEmpty(Collection collection){ return(collection == null || collection.isEmpty()); }

    /**
     * This method does nothing but break my program.
     * You're welcome.
     * @return
     */
    private Researchable getResearchable(){

        if(!isEmpty(researchableArrayList)){
            Random random = new Random();
            int index = random.nextInt(researchableArrayList.size());
            return researchableArrayList.get(index);
        }
        else {
            endGame();
            return null;
        }
    }

    /**
     * This method removes a researchable from the list
     * Takes a researchable parameter in order to ensure the proper researchable is being removed
     * @param researchable
     */
    //Getting fancy now aren't we?
    private void removeResearchable(Researchable researchable){
         researchableArrayList.remove(researchable);
     }

    /**
     * This method updates the UI to make sure that all the text is correct
     */
    private void updateButtons(){
        if(!gameOver){
            try {
                //Bleh
                Researchable researchable = getResearchable();
                //Boring
                button1.setText("Explore: %s".format(researchable.name()));
                buttonMap.put(button1, researchable);
                //Remove it
                researchableArrayList.remove(researchable);
                //Get it yet?
                researchable = getResearchable();
                //I hope you understand by now
                button2.setText("Explore: %s".format(researchable.name()));
                buttonMap.put(button2, researchable);
                //Almost there.....
                researchableArrayList.remove(researchable);
            }
            catch(NullPointerException ex){
                System.out.println("The list was empty!");
            }
            catch(Exception ex){
                System.out.println("Something went wrong!");
            }
        }
     }

     public void updateUI(Researchable researchable, Integer choice){
        switch(choice){
            case 1:
                textArea.appendText("You decide to explore the: " + researchable.name() + "\n");
                textArea.appendText(researchable.name() + "\n");
                textArea.appendText(researchable.description() + "\n");
                break;
            case 2:
                textArea.appendText("You decide to continue onwards and explore the rest of the ocean \n");
                break;
            case 3:
                textArea.appendText("Your mission is complete. You raise the ROV to the surface and decide to come back another day. \n");
                break;
        }
     }

    /**
     * This method handles progressing the story based on the button that was clicked
     * Takes one parameter for the button number to progress the story properly
     */
    private void progressStory(int buttonNumber){
        //Check to make sure that the game is not over
        if(!gameOver){
            if(buttonNumber == 1){
                //DEBUG purposes only
                //This is stupid, but it will eventually contain more of the story based on a branching system.
                textArea.appendText("\nThe first choice was clicked!\n");
                try {
                    Researchable researchable = buttonMap.get(button1);

                    buttonMap.remove(button1);
                }
                catch(NullPointerException ex) {
                    System.out.println("There was nothing in map or the item grabbed was null");
                }
                catch(Exception ex){
                    System.out.println("Something went wrong.");
                    System.out.println(ex.getMessage());
                }
            }
            else {
                //DEBUG purposes only
                try {
                    textArea.appendText("\nThe second choice was clicked!\n");
                    Researchable researchable = buttonMap.get(button2);
                    updateUI(researchable, 1);
                    buttonMap.remove(button2);
                }
                catch(NullPointerException ex) {
                    System.out.println("There was nothing in map or the item grabbed was null");
                }
                catch(Exception ex){
                    System.out.println("Something went wrong.");
                    System.out.println(ex.getMessage());
                }
            }

        }
    }

    /**
     * This method simply ends the game
     * Stop gawking. I know. It's amazing.
     */
    private void endGame() {
        gameOver = true;
        textArea.appendText("\n\nGAME OVER!!");
    }
}
