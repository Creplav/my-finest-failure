package sample;

/**
 * Created by Ben on 4/4/2017.
 * This is the base abstract class for all researchable objects
 */
public abstract class Researchable {

    /**
     * This method returns the name given to the object
     * Should be overridden
     * @return
     */
    public String name(){
        return "No Name Given";
    }

    /**
     * This method returns a description of the object
     * Should be overridden
     * @return
     */
    public String description(){
        //TODO Add an exception here for not overriding the method
        return "No Description";
    }

    public String backBoneType(){
        return "No Back Bone Given";
    }
}
