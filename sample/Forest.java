package sample;

import java.util.HashMap;

/**
 * Created by BCD on 4/8/2017.
 */
public class Forest {
    HashMap<String, Tree> trees = new HashMap<>();
    public void createTree(String name, Node rootNode){
        Tree tree = new Tree(rootNode);
        if(!trees.containsKey(name)){
            trees.put(name, tree);
        }
        else {
            System.out.println("Tree could not be created because that key already exists");
            System.out.println("Key: " + name);
        }
    }

    public Tree getTree(String key){
        if(trees.containsKey(key)){
            return trees.get(key);
        }
        else {
            System.out.println("Could not get the tree because the key for that tree does not exist");
            System.out.println("Key: " + key);
            return null;
        }
    }
}
