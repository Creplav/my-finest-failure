package sample;

import sample.animals.invertebrates.HermitCrab;
import sample.animals.invertebrates.Squid;
import sample.plants.RedAlgae;
import sample.plants.Seaweed;

/**
 * Created by BCD on 4/8/2017.
 */
public class StoryHandlerOld {
    Tree tree;
    public Node lastNode;
    Controller controller;
    public StoryHandlerOld(Tree tree, Controller controller)
    {
        this.tree = tree;
        this.controller = controller;
    }

    boolean checkForRoot(Node node){
        if(node.equals(tree.getRootNode())){
            return true;
        }
        else {
            return false;
        }
    }

    public Tree getTree(){
        return this.tree;
    }

    public void setLastNode(Node node){
        this.lastNode = node;
    }

    public Node getLastNode(){
        return this.lastNode;
    }

    /**
     * Runs the story for Explore Organism
     * Needs a number choice. Number choice being either 1 and 2
     * @param node
     * @param choice
     */
    void ExploreOrgansims(Node node, int choice){
       // setLastNode(node);
        if(choice != 1 && choice != 2){
            System.out.println("Invalid parameter 'choice'");
            return;
        }
        if(checkForRoot(node)){
            switch(choice){
                //If choice was organisms
                case 1:
                    System.out.println("You chose to search for organisms");
                    tree.addNode("SearchAnimals", new Node("Search for animals", node));
                    tree.addNode("SearchPlants", new Node("Search for plants", node));
                    controller.updateButton(tree.getNode("SearchAnimals").getObject().toString(), tree.getNode("SearchPlants").getObject().toString());
                    break;
                //If choice was ocean
                case 2:
                    System.out.println("You chose to explore some more of the ocean");
                    tree.addNode("HermitCrab", new Node(new HermitCrab(), node));
                    tree.addNode("Squid", new Node(new Squid(), node));
            }
        }
        else if(getLastNode().equals(node)){
            switch(choice){
                case 1:
                    tree.addNode("Vertebrates", new Node("Search for vertebrates", node));
                    tree.addNode("Invertebrates", new Node("Search for invertebrates", node));
                    controller.updateButton(tree.getNode("Vertebrates").getObject().toString(), tree.getNode("Invertebrates").getObject().toString());
                    break;
            }
        }
        else {
            System.out.println("Explore organism: node was not root!");
        }
    }

    void ExploreOcean(Node node, int choice){
        //setLastNode(node);
        if(choice != 1 && choice != 2){
            System.out.println("Invalid parameter 'choice'");
            return;
        }

        if(checkForRoot(node)){
            switch(choice){
                //Search for algae
                case 1:
                    System.out.println("You chose to seach for algae");
                    tree.addNode("RedAlgae", new Node(new RedAlgae(), node));
                    break;
                //Search for other plants
                case 2:
                    System.out.println("You chose to search for other plants");
                    tree.addNode("Seaweed", new Node(new Seaweed(), node));
                    break;
            }
        }

        else {
            System.out.println("Explore ocean: node was not root!");
        }
    }
}
