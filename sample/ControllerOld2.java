package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;

import java.net.URL;
import java.util.HashMap;
import java.util.ResourceBundle;

/**
 * This is the controller for the program.
 * It handles all of the FXML data and general program structure.
 * Potentially be broken up into a separate class.
 */
public class ControllerOld2 implements Initializable{

    /*
     * Reference all of the objects in the FXML file
     * That way we can use it later in the program
     */
    public TextArea textArea;
    public Button button1;
    public Button button2;

    public boolean gameOver = false;
    public boolean firstTime = true;

    public Forest forest;
    public Tree exploreOrganismTree;
    public Tree exploreOceanTree;
     Tree tree;

    StoryHandler storyHandler;

    Node lastNode = new Node(null);

    private HashMap<Node, Button> buttonHashMap = new HashMap<>();

    //Init method to handle the program loading
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //Start the story with some really cheesy wording
        textArea.setText("You move to the edge of the boat. \n\"Lower the ROV\", you say. " +
                "\nThe ROV is lowered into the water and you take the controls. It is a normal exploration mission. " +
                "Just a simple data collection mission. You begin your decent.... \n");
        forest = new Forest();
        Node exploreOrganismNode =  new Node("Explore Organism");
        Node exploreOceanNode = new Node("Explore Ocean");
        exploreOrganismNode.addChild(new Node("Search for vertebrates"));
        exploreOrganismNode.addChild(new Node("Search for invertebrates"));
        forest.createTree("ExploreOrganismTree",exploreOrganismNode);
        forest.createTree("ExploreOceanTree", exploreOceanNode);
        exploreOrganismTree = forest.getTree("ExploreOrganismTree");
        exploreOceanTree = forest.getTree("ExploreOceanTree");

        button1.setText(exploreOrganismTree.getRootNode().getObject().toString());
        button2.setText(exploreOceanTree.getRootNode().getObject().toString());
    }

    /**
     * This method handles button clicks based on an ActionEvent assigned to the buttons inside the FXML.
     * It checks to make sure which button you are pressing in order to continue the story
     */
    @FXML
    public void handleButtonClick(ActionEvent event){
        //Create a new button we can use as our clicked button
        Button clickedButton = new Button();
        //Create a new object that we will use for casting to the clicked button
        Object source = event.getSource();
        //Make sure that the ActionEvent is type of button
        if(source instanceof Button){
            //Cast the object to a button and set clickedButton equal to that
            clickedButton = (Button) source;
        }

        //Check to see if the clicked button was 1 or 2
        if(clickedButton.getId().equals("button1")) {

        }

        else if(clickedButton.getId().equals("button2")) {

        }
    }

    public void updateText(String text){
        textArea.appendText(text);
    }

    public void updateButton(String button1Text, String button2Text){
        button1.setText(button1Text);
        button2.setText(button2Text);
    }

    /**
     * This method simply ends the game
     * Stop gawking. I know. It's amazing.
     */
    private void endGame() {
        gameOver = true;
        textArea.appendText("\n\nGAME OVER!!");
    }
}

//
//    @FXML
//    public void handleButtonClick(ActionEvent event){
//        //Create a new button we can use as our clicked button
//        Button clickedButton = new Button();
//        //Create a new object that we will use for casting to the clicked button
//        Object source = event.getSource();
//        //Make sure that the ActionEvent is type of button
//        if(source instanceof Button){
//            //Cast the object to a button and set clickedButton equal to that
//            clickedButton = (Button) source;
//        }
//
//        //Check to see if the clicked button was 1 or 2
//        if(clickedButton.getId().equals("button1")){
//            if(firstTime){
//                tree = exploreOrganismTree;
//                storyHandler = new StoryHandler(tree, this);
//                storyHandler.setLastNode(exploreOrganismTree.getRootNode());
//                lastNode = storyHandler.getLastNode();
//                firstTime = false;
//            }
//            if(tree.equals(exploreOrganismTree)){
//                storyHandler.ExploreOrgansims(lastNode, 1);
//            }
//            else if(tree.equals(exploreOceanTree)){
//                storyHandler.ExploreOcean(lastNode, 1);
//            }
//            else {
//                System.out.println("Oh dear. Something went very wrong here. The tree is not either of the options!");
//            }
//        }
//        else if(clickedButton.getId().equals("button2")){
//            if(firstTime){
//                tree = exploreOceanTree;
//                storyHandler = new StoryHandler(tree, this);
//                storyHandler.setLastNode(exploreOceanTree.getRootNode());
//                lastNode = storyHandler.getLastNode();
//                firstTime = false;
//            }
//            if(tree.equals(exploreOrganismTree)){
//                storyHandler.ExploreOrgansims(lastNode, 2);
//            }
//            else if(tree.equals(exploreOceanTree)){
//                storyHandler.ExploreOcean(lastNode, 2);
//            }
//            else {
//                System.out.println("Oh dear. Something went very wrong here. The tree is not either of the options!");
//            }
//        }