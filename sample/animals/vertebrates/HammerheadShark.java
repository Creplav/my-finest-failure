package sample.animals.vertebrates;

import sample.Researchable;

/**
 * Created by Ben on 4/4/2017.
 * This is the class for the Hammerhead Shark.
 * It is a researchable object
 */
public class HammerheadShark extends Researchable {

    @Override
    public String name(){
        return "Hammerhead Shark";
    }
    @Override
    public String description() {
        return "\nA Hammerhead shark is a shark that needs help. Somehow it got a hammer on it's head.";
    }

    @Override
    public String backBoneType() {
        return "Vertebrate";
    }
}
