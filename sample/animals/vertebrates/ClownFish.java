package sample.animals.vertebrates;

import sample.Researchable;

/**
 * Created by BCD on 4/8/2017.
 */
public class ClownFish extends Researchable{
    @Override
    public String name() {
        return "Clown Fish";
    }

    @Override
    public String description() {
        return "This is a clown fish!";
    }

    @Override
    public String backBoneType() {
        return "Vertebrate";
    }
}
