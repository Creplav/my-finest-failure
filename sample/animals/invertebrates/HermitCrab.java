package sample.animals.invertebrates;

import sample.Researchable;

/**
 * Created by BCD on 4/8/2017.
 */
public class HermitCrab extends Researchable {
    @Override
    public String name() {
        return "Hermit Crab";
    }

    @Override
    public String description() {
        return "This is a cute little hermit crab";
    }

    @Override
    public String backBoneType() {
        return "Invertebrate";
    }
}
