package sample.animals.invertebrates;

import sample.Researchable;

/**
 * Created by BCD on 4/8/2017.
 */
public class Squid extends Researchable{

    @Override
    public String name() {
        return "Squid";
    }

    @Override
    public String description() {
        return "This is a tiny little squid";
    }

    @Override
    public String backBoneType() {
        return "Invertebrate";
    }
}
