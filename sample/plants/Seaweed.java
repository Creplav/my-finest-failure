package sample.plants;

import sample.Researchable;

/**
 * Created by BCD on 4/8/2017.
 */
public class Seaweed extends Researchable{
    @Override
    public String name() {
        return "Seaweed";
    }

    @Override
    public String description() {
        return "This is seaweed. You can eat it.";
    }
}
