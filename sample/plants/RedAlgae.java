package sample.plants;

import sample.Researchable;

/**
 * Created by Ben on 4/4/2017.
 */
public class RedAlgae extends Researchable {
    @Override
    public String name(){return "Red Algae"; }

    @Override
    public String description(){
        return "Red Algae grows in the sea because it is algae. Duh.";
    }
}
