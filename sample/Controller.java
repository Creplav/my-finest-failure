package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;

import java.net.URL;
import java.util.*;

/**
 * This is the controller for the program.
 * It handles all of the FXML data and general program structure.
 * Potentially be broken up into a separate class.
 */
public class Controller implements Initializable {

    /*
     * Reference all of the objects in the FXML file
     * That way we can use it later in the program
     */
    public TextArea textArea;
    public Button button1;
    public Button button2;

    Node rootNode = new Node("Root");
    Node parentNode;
    Node childNode;
    Tree tree = new Tree(rootNode);

    //Init method to handle the program loading
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        parentNode = rootNode;
        rootNode.addChild(new Node("Search For Animals", rootNode));
        rootNode.addChild(new Node("Search For Plants", rootNode));
    }

    /**
     * This method handles button clicks based on an ActionEvent assigned to the buttons inside the FXML.
     * It checks to make sure which button you are pressing in order to continue the story
     */
    @FXML
    public void handleButtonClick(ActionEvent event) {
        //Create a new button we can use as our clicked button
        Button clickedButton = new Button();
        //Create a new object that we will use for casting to the clicked button
        Object source = event.getSource();
        //Make sure that the ActionEvent is type of button
        if (source instanceof Button) {
            //Cast the object to a button and set clickedButton equal to that
            clickedButton = (Button) source;
        }

        //Check to see if the clicked button was 1 or 2
        if (clickedButton.getId().equals("button1")) {
            childNode = parentNode.getChild(0);
            parentNode = childNode;


        } else if (clickedButton.getId().equals("button2")) {
            parentNode.getChild(1);
            parentNode = childNode;
        }
    }
}
