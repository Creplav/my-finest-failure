package sample;

import java.util.HashMap;

//TODO Check to see if this works properly. It may not because of the branches.
//TODO Find a way to connect the nodes

/**
 * Created by BCD on 4/8/2017.
 * Creates a new tree that contains branches and nodes
 */
public class Tree {
    Node rootNode;
    HashMap<String, Node> children = new HashMap<>();

    public Tree(Node rootNode){
        this.rootNode = rootNode;
    }


    public void addNode(String name, Node node){
        children.put(name, node);
    }

    public Node getNode(String key){
        if(children.containsKey(key)){
            return children.get(key);
        }
        else {
            System.out.println("This key does not exist: " + key);
            return null;
        }
    }

    public Node getRootNode(){
        return this.rootNode;
    }
}
