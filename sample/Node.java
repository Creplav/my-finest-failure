package sample;

import java.util.ArrayList;

/**
 * Created by BCD on 4/8/2017.
 */
public class Node {
    Object nodeObject;
    Node parent;

    ArrayList<Node> children = new ArrayList<>();

    public Node(Object nodeObject){
        this.nodeObject = nodeObject;
    }

    public Node(Object node, Node parent){
        this.nodeObject = node;
        this.parent = parent;
    }

    public Node(String string){
        this.nodeObject = string;
    }

    public Object getObject(){
        return this.nodeObject;
    }

    public Node getParent(){
        if(this.parent != null){
            return this.parent;
        }
        else {
            System.out.println("This node does not contain a parent.");
            return null;
        }
    }

    public void addChild(Node child){
        children.add(child);
    }

    public Node getChild(int index){
        return children.get(index);
    }
}
