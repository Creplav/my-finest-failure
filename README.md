# my-finest-failure

### My absolute finest failure of a project

So I was going through some of my old stuff today and I found this project I tried to make for university 2 years ago. 

My comments were very... entertaining. When I was making this project I clearly had no idea what I was doing and I ended up making a huge mess of everything. 

I highly encourage you to look through this and maybe get a smile out of it. 

It's also good to look back at what I did and see how far I have come since then. 
Feel free to laugh with me at the horrible design of this project. 
