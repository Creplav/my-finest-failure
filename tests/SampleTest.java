package tests;

import sample.*;
import sample.plants.RedAlgae;
import sample.animals.vertebrates.HammerheadShark;

/**
 * Created by BCD on 4/7/2017.
 *
 * This is the UnitTest for the program. 
 */
public class SampleTest {

    public static void main(String[] args) {
//        ControllerOld controller = new ControllerOld();
//        HammerheadShark hs = new HammerheadShark();
//        RunCollectionTest(controller.researchableArrayList, hs);
        runForestTest();
    }

    /**
     * This method tests to see if a collection is empty
     */
//    private static void RunCollectionTest(Collection collection, Object object){
//        collection.add(object);
//        try{
//            assert ControllerOld.isEmpty(collection);
//            System.out.println("The collection was empty");
//        }
//        catch(AssertionError ex){
//            System.out.println("The collection was not empty!");
//        }
//        catch(Exception ex){
//            System.out.println("Something went wrong!");
//        }
//
//    }

    //TODO make sure that everything works properly here
    public static void runForestTest(){
        Forest forest = new Forest();
        Node tree1Root = new Node("Tree1Root");
        Node tree2Root = new Node("Tree2Root");
        forest.createTree("Tree1", tree1Root);
        forest.createTree("Tree2", tree2Root);
        Tree tree1 = forest.getTree("Tree1");
        Tree tree2 = forest.getTree("Tree2");

        tree1.getRootNode().addChild(new Node("Node1"));
        tree1.getRootNode().addChild(new Node("Node2"));
        Node node2 = tree1.getRootNode().getChild(0);
        Node node3 = tree1.getRootNode().getChild(1);
        node2.addChild(new Node("Poopy"));
        node2.addChild(new Node("Ding"));
        Node node4 = node2.getChild(0);
        Node node5 = node2.getChild(1);
        node4.addChild(new Node("Stupid"));
        node4.addChild(new Node("jaskjs"));
        Node node6 = node4.getChild(0);
        Node node7 = node4.getChild(1);
        node6.addChild(new Node(new HammerheadShark()));
        Node node8 = node6.getChild(0);

        System.out.println(node2.getObject());
        System.out.println(node3.getObject());
        System.out.println(node4.getObject());
        System.out.println(node5.getObject());
        System.out.println(node6.getObject());
        System.out.println(node7.getObject());
        System.out.println(node8.getObject());

    }

}
